import { Tags } from './tags.enum';
export class Goal {

    private _title : string;
    private _description : string;
    private _tags : Tags[];
    
    constructor(
        title : string, description : string, tags : Tags[]
    ) {
        this._title = title;
        this._description = description;
        this._tags = tags;
    }

    get title() : string {
        return this._title;
    }
    
    set title(title : string) {
        this._title = title;
    }

    get description() : string {
        return this._description;
    }

    set description(description : string) {
        this._description = description;
    }

    get tags() : Tags[] {
        return this._tags;
    }

    set tags(tags : Tags[]) {
        this._tags = tags;
    }

}
