import { Component, OnInit } from '@angular/core';

import { Tags } from '../models/tags.enum';
import { GoalsService } from './../goals.service';

@Component({
  selector: 'app-user-tags',
  templateUrl: './user-tags.component.html',
  styleUrls: ['./user-tags.component.css']
})
export class UserTagsComponent implements OnInit {

  tagsCount : number[] = [];
  tagSelected : number = Tags.ALL;

  constructor(private goalsService : GoalsService) { }

  ngOnInit() {
    for (let item in Tags) {
      if (isNaN(Number(item))) {
        this.tagsCount.push(5);//mock
      }
    }
  }

  getTagName(idx : number) : string {
    return Tags[idx].replace("_", " ");
  }

  /**
   * Klikamy w tag.
   * 1) Zapisujemy o tym informacje aby zmienić styl.
   * 2) Wysyłamy informacje do innych komponentow przez goalsService.
   */
  tagClick(tagId : number) {
    this.tagSelected = tagId;
    this.goalsService.sendTag(tagId);
  }

}
