import { GoalsService } from './goals.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ClarityModule } from '@clr/angular';
import { FormsModule }   from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { GoalCardComponent } from './goal/goal-card/goal-card.component';
import { GoalsComponent } from './goal/goals/goals.component';
import { UserTagsComponent } from './user-tags/user-tags.component';
import { GoalFormComponent } from './goal/goal-form/goal-form.component';
import { ModalOpenerService } from './goal/goal-form/modal-opener.service';


@NgModule({
  declarations: [
    AppComponent,
    GoalCardComponent,
    GoalsComponent,
    UserTagsComponent,
    GoalFormComponent
  ],
  imports: [
    BrowserModule,
    ClarityModule,
    HttpModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [GoalsService, ModalOpenerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
