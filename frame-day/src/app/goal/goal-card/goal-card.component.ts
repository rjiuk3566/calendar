import { Tags } from '../../models/tags.enum';
import { Component, OnInit, Input } from '@angular/core';
import { Goal } from './../../models/goal';
import { animateFactory } from 'ng2-animate';

@Component({
  selector: 'app-goal-card',
  templateUrl: './goal-card.component.html',
  styleUrls: ['./goal-card.component.css'],
  animations: [animateFactory(300, 0, 'ease-in')]
})
export class GoalCardComponent implements OnInit {

  @Input()goal  : Goal;
  @Input()selectedTag : number;

  constructor() { }

  ngOnInit() {
  }

  getTagName(idx : number) : string {
    return Tags[idx].replace("_", " ");
  }


}
