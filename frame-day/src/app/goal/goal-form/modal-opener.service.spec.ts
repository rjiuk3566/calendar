import { TestBed, inject } from '@angular/core/testing';

import { ModalOpenerService } from './modal-opener.service';

describe('ModalOpenerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalOpenerService]
    });
  });

  it('should be created', inject([ModalOpenerService], (service: ModalOpenerService) => {
    expect(service).toBeTruthy();
  }));
});
