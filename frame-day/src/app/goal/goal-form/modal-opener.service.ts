import { Goal } from '../../models/goal';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ModalOpenerService {

  constructor() { }

  /**
   * Zwracamy numer info aby wlaczyc modala z celem
   */
   private subjectGoal = new Subject<Goal>();
 
    sendGoalToForm(goal: Goal) {
        this.subjectGoal.next(goal);
    }
 
    getGoalToForm(): Observable<Goal> {
        return this.subjectGoal.asObservable();
    }


}
