import { Component, Input, OnInit } from '@angular/core';
import { Goal } from './../../models/goal';
import { ModalOpenerService } from './modal-opener.service';

@Component({
  selector: 'app-goal-form',
  templateUrl: './goal-form.component.html',
  styleUrls: ['./goal-form.component.css']
})
export class GoalFormComponent implements OnInit {

  @Input() goal : Goal = new Goal("", "", []) ;
  open : boolean;

  constructor(private modalOpenerService : ModalOpenerService) { 
    modalOpenerService.getGoalToForm().subscribe(goal => {
      this.goal = goal;
      this.open = true;
    });
  }

  ngOnInit() {
  }

}
