import { Component, OnInit, HostListener } from '@angular/core';
import { GoalsService } from './../../goals.service';
import { Goal } from './../../models/goal';
import { Subscription } from 'rxjs/Subscription';
import { animateFactory } from 'ng2-animate';
import { ModalOpenerService } from './../goal-form/modal-opener.service';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.css'],
  animations: [animateFactory(300, 0, 'ease-in')]
})
export class GoalsComponent implements OnInit {

  private preFilterGoals : Goal[];
  private goals : Goal[];
  private ROWS : number = 8;
  private selectedTag : number;
  goalRows : Goal[][];

  tagSub: Subscription;

  constructor(private goalsService : GoalsService, private modalOpenerService : ModalOpenerService) { 
    //pobieramy cele
    goalsService.getGoals().subscribe(
      val => { this.preFilterGoals = val; this.goals = val;},
      console.error,
      () => this.makeRowsForWindow(window.innerWidth)
    );

  }

  ngOnInit() {
    this.goalRows = this.redoGoalsForRows(this.goals);
    this.goalsService.getTag().subscribe(tag => this.filterByTag(tag));
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.makeRowsForWindow(event.target.innerWidth);
  }

  filterByTag(tag : number) : void {
    this.goals = this.preFilterGoals.filter(goal => {
      if(tag === 0) {
        return true;
      }
      //jesli zawiera tak, lub ALL(czyli jest wszedzie)
      return goal.tags.includes(tag) || goal.tags.includes(0);
    });

    this.selectedTag = tag;
    this.goalRows = this.redoGoalsForRows(this.goals);
  }

  makeRowsForWindow(width : number) {;
    this.ROWS = this.calculateRows(width);
    console.log(this.ROWS);
    this.goalRows = this.redoGoalsForRows(this.goals);
  }

  calculateRows(width : number) {
    return Math.floor(width / 350); 
  }

  redoGoalsForRows(goals : Goal[]) : Goal[][] {
    let goalRows : Goal[][] = [];

    let accCol = 0;
    let accRow : Goal[] = [];
    for (let goal of goals) {
      if(accCol == 0) {//nowy wiersz
        accRow = [];
      }

      accRow.push(goal);
      accCol = accCol + 1;

      if(accCol == this.ROWS) {//nowy wiersz
        goalRows.push(accRow);
        accCol = 0;
      }
    }

    if(accRow.length != 0) {
      goalRows.push(accRow);
    }

    return goalRows;
  }

  /**
   * Klikanie i edycja celi
   */
  newGoalClicked() {
    this.clickGoal(new Goal("", "", []))
  }

  clickGoal(clickedGoal : Goal) {
    let newGoal : Goal = this.goalsService.copyGoal(clickedGoal);
    this.modalOpenerService.sendGoalToForm(newGoal);
  }
}

