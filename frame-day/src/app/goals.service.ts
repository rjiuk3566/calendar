import { Tags } from './models/tags.enum';
import { Observable } from 'rxjs/Rx';
import { observable } from 'rxjs/symbol/observable';
import { Injectable, Output, EventEmitter  } from '@angular/core';
import { Goal } from './models/goal';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class GoalsService {

    GOALS : Goal[] =  [
      new Goal("", "", [Tags.ALL]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.FAMILY]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.HEALTH, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.HEALTH, Tags.PERSONAL_DEVELOPMENT]),
       new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.FAMILY]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.HEALTH, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.HEALTH, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.HEALTH, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.FAMILY]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.FAMILY]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT]),
      new Goal("Nauka angielskiego", "nauka angielskiego z fiszkami", [Tags.EDUCATION, Tags.PERSONAL_DEVELOPMENT])
    ];

  constructor() { }

  /**
   * Pierwszy goal musi być pusty aby wstawić karte z PLUSIKIEM : )
   */
  getGoals() : Observable<Goal[]> {
    return of(this.GOALS);
  }

  /**
   * Zwracamy numer taga do filtrowania, 0 oznacza WSZYSTKO
   */
   private subjectTags = new Subject<number>();
 
    sendTag(tag: number) {
        this.subjectTags.next(tag);
    }
 
    getTag(): Observable<any> {
        return this.subjectTags.asObservable();
    }

    /**
     * Kopiowania celu
     */
    copyGoal(source : Goal) : Goal {
        return new Goal(
            source.title,
            source.description,
            source.tags
        );
    }
}
  